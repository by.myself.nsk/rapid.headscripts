<?php

use Bitrix\Main\Loader;

Loader::includeModule('iblock');
Loader::includeModule('rapid.usertypes');

Loader::registerNamespace("Rapid\\Headscript", __DIR__ . '/lib');

