<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;


Class Rapid_Headscripts extends CModule
{
    public function __construct()
    {
        Loc::loadMessages(__FILE__);

        $this->fillModuleFields();
    }

    private function fillModuleFields()
    {
        $this->path = $this->getModulePath();
        $this->MODULE_ID = $this->getModuleIdFromModulePath($this->path);
        $this->MODULE_NAME = GetMessage('RAPID_HEADSCRIPTS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('RAPID_HEADSCRIPTS_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage('RAPID_HEADSCRIPTS_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('RAPID_HEADSCRIPTS_PARTNER_URI');
        $arModuleVersion = [];
        /** @noinspection PhpIncludeInspection */
        include($this->path . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
    }

    private function getModulePath()
    {
        return dirname(__FILE__);
    }

    private function getModuleIdFromModulePath($modulePath)
    {
        $modulePath = substr($modulePath, strlen($_SERVER['DOCUMENT_ROOT']));
        $arPaths = explode(DIRECTORY_SEPARATOR, $modulePath);
        $pos = array_search('modules', $arPaths);
        $modulePos = $pos + 1;
        $moduleId = $arPaths[$modulePos];
        return $moduleId ?: '';
    }

    public function DoInstall()
    {
        global $APPLICATION, $step, $errors;

        $errors = [];

        if (!CModule::IncludeModule('iblock')) {
            $errors[] = Loc::getMessage('RAPID_HEADSCRIPTS_REQUIRE_IBLOCK_MODULE');
        }
        if (!ModuleManager::isModuleInstalled('rapid.usertypes')) {
            $errors[] = Loc::getMessage('RAPID_HEADSCRIPTS_REQUIRE_USERTYPES_MODULE');
        }
        if (!$errors) {
            $this->InstallFiles();
            $this->InstallIblock();
            $this->InstallEvents();
        }

        if (!$errors) {
            RegisterModule($this->MODULE_ID);
        } else {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('RAPID_HEADSCRIPTS_RESULT_PAGE_TITLE'),
                __DIR__ . '/install_result.php'
            );
        }
    }

    public function InstallFiles()
    {
        return $this;
    }

    public function InstallIblock()
    {
        global $DB, $errors;

        $iblockType = 'rapid_headscripts';
        $iblockCode = 'rapid_headscripts';
        $iblockName = 'Rapid: Скрипты в шапке';
        $sort = 10000;

        $arSiteIds = self::getSiteIds();
        $arLangIds = self::getLangIds();

        try {
            $DB->StartTransaction();
            $arIblockType = CIBlockType::GetList([], [
                'ID' => $iblockType,
            ])->Fetch();
            if (!$arIblockType) {
                $obIblockType = new CIBlockType();

                $lang = array_fill_keys($arLangIds, [
                    'NAME' => $iblockName,
                    'SECTION_NAME' => $iblockName,
                    'ELEMENT_NAME' => $iblockName,
                ]);

                if (!$obIblockType->Add([
                    'ID' => $iblockType,
                    'SECTIONS' => 'N',
                    'IN_RSS' => 'N',
                    'SORT' => $sort,
                    'LANG' => $lang,
                ])) {
                    throw new Exception($obIblockType->LAST_ERROR, 1);
                }
                $arIblockType = CIBlockType::GetList([], [
                    'ID' => $iblockType,
                ])->Fetch();
            }

            $arIblock = CIBlock::GetList([], [
                'CODE' => $iblockCode
            ])->Fetch();
            if (!$arIblock) {
                $obIblock = new CIBlock();
                if (!$obIblock->Add([
                    'IBLOCK_TYPE_ID' => $arIblockType['ID'],
                    'CODE' => $iblockCode,
                    'NAME' => $iblockName,
                    'SITE_ID' => $arSiteIds,
                ])) {
                    throw new Exception($obIblock->LAST_ERROR, 2);
                }
                $arIblock = CIBlock::GetList([], [
                    'CODE' => $iblockCode
                ])->Fetch();
            }

            $obProperty = new CIBlockProperty();
            if (!$obProperty->Add([
                'IBLOCK_ID' => $arIblock['ID'],
                'CODE' => 'SITE_ID',
                'SORT' => '10',
                'NAME' => 'Сайт',
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'SiteId',
                'IS_REQUIRED' => 'Y',
            ])) {
                throw new Exception($obProperty->LAST_ERROR, 3);
            }
            if (!$obProperty->Add([
                'IBLOCK_ID' => $arIblock['ID'],
                'CODE' => 'TEXT_HEAD',
                'SORT' => '20',
                'NAME' => 'Текст в head',
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'HTML',
            ])) {
                throw new Exception($obProperty->LAST_ERROR, 4);
            }
            if (!$obProperty->Add([
                'IBLOCK_ID' => $arIblock['ID'],
                'CODE' => 'TEXT_BODY',
                'SORT' => '30',
                'NAME' => 'Текст в body',
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'HTML',
            ])) {
                throw new Exception($obProperty->LAST_ERROR, 5);
            }

            $DB->Commit();
        } catch (Exception $e) {
            $errors = '[' . $e->getCode() . '] ' . $e->getMessage();
            $DB->Rollback();
        }

        return $this;
    }

    protected static function getSiteIds()
    {
        $rsSites = CSite::GetList($by, $order, []);
        $arSiteIds = [];
        while ($arSite = $rsSites->Fetch()) {
            $arSiteIds[$arSite['ID']] = $arSite['ID'];
        }
        return $arSiteIds;
    }

    protected static function getLangIds()
    {
        $rsSites = CLanguage::GetList($by, $order, []);
        $arSiteIds = [];
        while ($arSite = $rsSites->Fetch()) {
            $arSiteIds[$arSite['ID']] = $arSite['ID'];
        }
        return $arSiteIds;
    }

    public function InstallEvents()
    {
        RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID);
        return $this;
    }

    public function DoUninstall()
    {
        $this
            ->UnInstallIblock()
            ->UnInstallEvents()
            ->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }

    public function UnInstallIblock()
    {
        global $DB, $errors;

        $iblockType = 'rapid_headscripts';

        $arIblockType = CIBlockType::GetList([], [
            'ID' => $iblockType,
        ])->Fetch();
        if (!$arIblockType) {
            return $this;
        }
        try {
            $DB->StartTransaction();
            if (!CIBlockType::Delete($iblockType)) {
                throw new Exception();
            }
            $DB->Commit();
        } catch (Exception $e) {
            $errors = $e->getMessage();
            $DB->Rollback();
        }
        return $this;
    }

    public function UnInstallEvents()
    {
        global $DB;
        $dbModuleDepends = $DB->Query('SELECT * FROM b_module_to_module WHERE TO_MODULE_ID = "' . $this->MODULE_ID . '"');
        while ($arModuleDepend = $dbModuleDepends->Fetch()) {
            UnRegisterModuleDependences(
                $arModuleDepend['FROM_MODULE_ID'],
                $arModuleDepend['MESSAGE_ID'],
                $arModuleDepend['TO_MODULE_ID'],
                $arModuleDepend['TO_CLASS'],
                $arModuleDepend['TO_METHOD']
            );
        }
        return $this;
    }

    public function UnInstallFiles()
    {
        return $this;
    }

}
