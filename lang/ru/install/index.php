<?php
/** @var array $MESS */
$MESS = array_merge($MESS, array(
    'RAPID_HEADSCRIPTS_MODULE_NAME' => 'Rapid: Скрипты в шапке',
    'RAPID_HEADSCRIPTS_MODULE_DESCRIPTION' => 'Позволяет редактировать подключаемые дополнительные скрипты в head/body сайта',
    'RAPID_HEADSCRIPTS_PARTNER_NAME' => 'Rapid',
    'RAPID_HEADSCRIPTS_PARTNER_URI' => '',
    'RAPID_HEADSCRIPTS_REQUIRE_IBLOCK_MODULE' => 'Для установки необходим установленный модуль iblock',
    'RAPID_HEADSCRIPTS_REQUIRE_USERTYPES_MODULE' => 'Для установки необходим установленный модуль rapid.usertypes',
    'RAPID_HEADSCRIPTS_RESULT_PAGE_TITLE' => 'Установка модуля Rapid: Скрипты в шапке',
));
