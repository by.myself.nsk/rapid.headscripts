<?

namespace Rapid\Headscript;

use Bitrix\Main\Page\Asset;
use CIBlock;
use CIBlockElement;
use CPHPCache;
use CSite;
use Exception;

class Headscript
{
    protected static $optionIblockIdCode = 'rapid_headscripts_iblock_id';

    protected static $instance;
    protected $data = [];

    protected static function getModuleId()
    {
        return 'rapid.headscripts';
    }

    protected static function getIblockId($siteId = SITE_ID)
    {
        $arIblock = CIBlock::GetList([], [
            'TYPE' => 'rapid_headscripts',
            'CODE' => 'rapid_headscripts'
        ])->Fetch();
        return $arIblock['ID'];
    }

    protected static function getElementList()
    {
        global $CACHE_MANAGER;
        $obCache = new CPHPCache();

        $method = str_replace(['\\', '::'], DIRECTORY_SEPARATOR, __METHOD__);
        $strCachePath = implode(DIRECTORY_SEPARATOR, [
            self::getSiteIdOrDefaultSiteId(),
            $method
        ]);
        $strCachePath = preg_replace('/\\' . DIRECTORY_SEPARATOR . '{2,}/', DIRECTORY_SEPARATOR, $strCachePath);
        $strCacheId = str_replace(DIRECTORY_SEPARATOR, '_', trim($strCachePath, DIRECTORY_SEPARATOR));
        $cacheTime = 31536000; // year

        if ($obCache->InitCache($cacheTime, $strCacheId, $strCachePath)) {
            $result = $obCache->GetVars();
        } elseif ($obCache->StartDataCache($cacheTime, $strCacheId, $strCachePath)) {
            $CACHE_MANAGER->StartTagCache($strCachePath);
            try {
                $iblockId = self::getIblockId();

                $rsElements = CIBlockElement::GetList(
                    ['SORT' => 'ASC'],
                    [
                        'IBLOCK_ID' => $iblockId,
                        'ACTIVE' => 'Y',
                    ],
                    false,
                    false,
                    [
                        'IBLOCK_ID',
                        'ID',
                        'NAME',
                        'CODE',
                        'SORT',
                        'PROPERTY_SITE_ID',
                        'PROPERTY_TEXT_HEAD',
                        'PROPERTY_TEXT_BODY',
                    ]
                );
                $arElements = [];
                while ($arElement = $rsElements->Fetch()) {
                    $id = $arElement['ID'];
                    $siteId = $arElement['PROPERTY_SITE_ID_VALUE'];
                    $arElements[$siteId][$id] = [
                        'NAME' => $arElement['NAME'],
                        'CODE' => $arElement['CODE'],
                        'SORT' => $arElement['SORT'],
                        'SITE_ID' => $siteId,
                        'TEXT_HEAD' => $arElement['PROPERTY_TEXT_HEAD_VALUE']['TEXT'],
                        'TEXT_BODY' => $arElement['PROPERTY_TEXT_BODY_VALUE']['TEXT'],
                    ];
                }
                $result = $arElements;

                $CACHE_MANAGER->RegisterTag('iblock_id_' . $iblockId);
                $CACHE_MANAGER->EndTagCache();
                $obCache->EndDataCache($result);
            } catch (Exception $e) {
                $obCache->abortDataCache();
            }
        }
        return $result;
    }

    protected static function getSiteIdOrDefaultSiteId()
    {
        $bAdmin = CSite::InDir('/bitrix/') && !CSite::InDir('/bitrix/templates/');
        $isDefinedConst = (defined('ADMIN_SECTION') && ADMIN_SECTION === true);

        if (($isDefinedConst || $bAdmin)) {
            return CSite::GetDefSite();
        }
        return SITE_ID;
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }
        self::$instance = new self();
        return self::$instance;
    }

    protected function __construct()
    {
        $data = self::getElementList();
        $this->data = $data[SITE_ID];
    }

    public function showHeadScripts()
    {
        foreach ($this->data as $item) {
            echo $item['TEXT_HEAD'];
        }
    }

    public function showBodyScripts()
    {
        foreach ($this->data as $item) {
            echo $item['TEXT_BODY'];
        }
    }
}
